# -*- coding: utf-8 -*-

from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$Date: 2015-03-15 05:56:36 +0300 (Sun, 15 Mar 2015) $'


import traceback
import sys
import glob
import os

from preference.settings import EXTENSION, style, params_db, config_path, DEFAULT_TOR_PATH_ID
from viewer.main_form import APP, MainWindow
from preference.logger import LOGGER
from tools.db_connection import sql_exec, sql_exec_script
from preference.querys import get_tor_path, NEW_DB
from viewer.create_add_form import show_error


def main():
    app = APP(sys.argv)
    app.setStyle(style['default'])
    try:
        app.setStyleSheet(open(style['main_theme'], "r").read())
    except IOError:
        pass

    # Initiate main form.
    dialog_form = MainWindow()
    dialog_form.set_top_level_window()
    dialog_form.raise_()
    dialog_form.show()
    dialog_form.move_to_center()
    dialog_form.main_form.find_torrent_file()  # Find new torrent file.
    dialog_form.main_form.set_current_index()  # Set auto-visible index (row).

    # Make window information open or close.
    if not dialog_form._static:
        dialog_form.set_slide_window_button()
    sys.exit(app.exec_())


def pre_start():
    """Проверка существования БД конфигураций."""
    if not os.path.exists(params_db):
        if not os.path.exists(config_path):
            os.makedirs(config_path)

        LOGGER.warning('Конфигурационная БД отсутствует, будет создана новая: %s', params_db)
        try:
            sql_exec_script(NEW_DB)
            LOGGER.info("Database created and opened succesfully")
        except Exception as err:
            LOGGER.error('Не предвиденная ошибка при создании БД: %s', err)
            LOGGER.exception(err)


if __name__ == '__main__':
    LOGGER.debug('Запуск программы.')
    try:
        pre_start()
        torrent_files = glob.glob(os.path.join(sql_exec(get_tor_path.format(DEFAULT_TOR_PATH_ID)), EXTENSION))
        if torrent_files:
            LOGGER.debug('Added New Torrents: %s' % torrent_files)
            main()
        else:
            main()
    except Exception as ex:
        LOGGER.error('Непредвиденная ошибка: %s', ex)
        LOGGER.error(traceback.print_exc())
        show_error(ex)
