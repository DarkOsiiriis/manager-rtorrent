# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = '$:'
__date__ = '$:'


from PyQt5 import QtWidgets, QtCore
from manage_rtorrent.preference.settings import size_file_info as sfi
from shortcuts import GlobalShortcuts


class ShowTorrentInfo(QtWidgets.QListWidget, GlobalShortcuts):
    def __init__(self, parent=None):
        # Работает только так, через __init__  parent=None, пока разбираться не хочу, но нужно.
        QtWidgets.QListWidget.__init__(self, parent=None)
        GlobalShortcuts.__init__(self, parent=None)
        self.setGeometry(QtCore.QRect(sfi['x'], sfi['y'], sfi['width'], sfi['height']))
        self.setObjectName('Show Tor Info')
        self.setCurrentRow(2)
        self.item(2)
        self.setSortingEnabled(True)

    def move_to_center(self):
        """
            Move window to center screen.
        """
        screen = QtWidgets.QApplication.desktop()
        size = self.frameSize()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def set_top_level_window(self):
        """
            Set strong on top window on start.
        """
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized | QtCore.Qt.WindowActive)
        self.activateWindow()
