# -*- coding: utf8 -*-


from PyQt5 import QtWidgets, QtCore
from manage_rtorrent.preference.logger import LOGGER
from manage_rtorrent.tools.db_connection import sql_exec
from manage_rtorrent.preference import querys
from manage_rtorrent.resources.lang import messages as msg
from manage_rtorrent.preference import settings
from manage_rtorrent.version import VERSION
from preference_form import PreferenceForm
from shortcuts import GlobalShortcuts

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class CreateMenu(GlobalShortcuts):
    def __init__(self, parent=None):
        super(CreateMenu, self).__init__(parent)
        self.pref = PreferenceForm()
        self.prop_menu = self.menuBar().addMenu("&Property")
        self.file_menu = self.menuBar().addMenu("&File")
        self.view_menu = self.menuBar().addMenu("&View")
        self.help_menu = self.menuBar().addMenu("&Help")

    def create_actions(self):
        """Действия для меню и шорткатов"""
        self.refresh_act = QtWidgets.QAction("&Refind Files", self, shortcut="Ctrl+R",
                                             triggered=self.refresh_table)
        self.open_act = QtWidgets.QAction("Show New Files", self, shortcut="Ctrl+O",
                                          triggered=self.show_dir_new_files)
        self.exit_act = QtWidgets.QAction("E&xit", self, shortcut="Ctrl+W", statusTip=_fromUtf8('Выход из программы'),
                                          triggered=self.close)
        self.about_act = QtWidgets.QAction("&About", self, statusTip=_fromUtf8('Показать информацию о программе'),
                                           triggered=self.about)
        self.prop_act = QtWidgets.QAction("&Preferences", self, shortcut="Ctrl+Shift+P",
                                          triggered=self.property_main_window)
        self.change_state_act = QtWidgets.QAction("&Change State", self, shortcut="Ctrl+E",
                                                  triggered=self.change_state)
        self.about_app_act = QtWidgets.QAction("&About", self, triggered=self.about)

    def refresh_table(self, arg=None):
        """Перезапись таблицы, функция для перезаписи метода основным наследуемым классом"""

    def change_state(self):
        """Изменение состояния триггера, функция для перезаписи метода основным наследуемым классом"""

    def create_menu(self):
        self.file_menu.addAction(self.open_act)
        self.file_menu.addSeparator()
        self.file_menu.addAction(self.exit_act)
        self.prop_menu.addAction(self.prop_act)
        self.view_menu.addAction(self.refresh_act)
        self.view_menu.addAction(self.change_state_act)
        self.help_menu.addAction(self.about_act)
        self.help_menu.addAction(self.about_app_act)

    def show_dir_new_files(self):
        """Действие открытия файла"""
        dir_path = sql_exec(querys.get_tor_path.format(settings.DEFAULT_TOR_PATH_ID))
        file_name = QtWidgets.QFileDialog.getOpenFileNames(self, "Показать новые Torrent файлы", dir_path,
                                                           "Torrent (*.torrent)")

    def about(self):
        """Метод показывающий информацию о программе"""
        QtWidgets.QMessageBox.about(self, "About Menu", msg.ABOUT.format(
            (settings.PROGRAM_NAME, settings.color['orange']),
            (VERSION, settings.color['green'])))

    def set_settings_object(self, setng):
        self.edit_ini.load_file(setng.fileName())
        self.edit_ini.show()

    def property_main_window(self):
        pass
        self.pref.show()
        # self.pref.move_to_center()
        self.pref.setModal(True)
        self.pref.setLayout(self.pref.main_layout)




