# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = '$:'
__date__ = '$:'


from PyQt5.QtWidgets import QTableView, QAbstractItemView
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import Qt
from manage_rtorrent.preference.settings import size


class TableForm(QStandardItemModel, QTableView):
    def __init__(self, parent=None):
        super(TableForm, self).__init__(parent)

        self.item_model = QStandardItemModel(0, 0, parent)
        self.table = QTableView()
        self.table.setFixedWidth(size['offset'])
        self.table.setObjectName('Torrent Info')

        self.set_table_property()

        # Создание таблицы в QT.
        # Задаем заголовок строки.
        self.item_model.setHorizontalHeaderItem(0, QStandardItem('Path'))
        self.item_model.setHorizontalHeaderItem(1, QStandardItem('Name'))
        self.item_model.setHorizontalHeaderItem(2, QStandardItem('Url'))
        # Можно здесь, а можно в функции вывести таблицу.
        self.table.setModel(self.item_model)
        self.set_size_column()

    def set_table_property(self):
        """Задать свойстка отображения таблицы"""
        # Включаем чередующуюся подсветку строк.
        self.table.setAutoFillBackground(True)
        self.table.setAlternatingRowColors(True)
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.verticalHeader().setVisible(False)
        self.table.setWordWrap(True)  # Перенос длинной строки на новую строку.
        self.table.setSortingEnabled(True)
        self.table.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.table.setContextMenuPolicy(Qt.CustomContextMenu)

    def set_size_column(self):
        # Непосредственное отображение строк и колонок.
        self.table.setColumnWidth(0, 50)
        self.table.setColumnHidden(0, True)  # Hide Column.
        self.table.setColumnWidth(1, 250)
        self.table.setColumnWidth(2, 80)

    def clear_table(self):
        """Очистка таблицы"""
        self.item_model.clear()
        self.item_model.setHorizontalHeaderItem(0, QStandardItem('Path'))
        self.item_model.setHorizontalHeaderItem(1, QStandardItem('Name'))
        self.item_model.setHorizontalHeaderItem(2, QStandardItem('Url'))
        self.set_table_property()
        self.set_size_column()

    def _test(self):
        self.table.getContentsMargins()
