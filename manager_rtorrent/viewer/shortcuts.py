# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = '$:'
__date__ = '$:'

from PyQt5.QtWidgets import QAction
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt


class GlobalShortcuts(QAction):
    def __init__(self, parent=None):
        super(GlobalShortcuts, self).__init__(parent)
        self.act_close = QAction('Close window', self)
        self.act_close.setShortcut(QKeySequence("Ctrl+W"))
        self.act_close.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.act_close)

        self.act_close.triggered.connect(self.close)

        self.slide_act = QAction('Open and close slide window', self)
        self.slide_act.setShortcut(QKeySequence("Ctrl+S"))
        self.slide_act.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.slide_act)
        # self.slide_act.triggered.connect(self.set_slide_window_button)

        self.refresh_act = QAction('Refind new files', self)
        self.refresh_act.setShortcut(QKeySequence("Ctrl+R"))
        self.refresh_act.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.refresh_act)

        self.select_all_act = QAction('Select all items', self)
        self.select_all_act.setShortcut(QKeySequence("Ctrl+A"))
        self.select_all_act.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.select_all_act)

        self.del_file_act = QAction('Select all items', self)
        self.del_file_act.setShortcut(QKeySequence("Ctrl+Backspace"))
        self.del_file_act.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.del_file_act)

        self.show_info_act = QAction('Show info of file', self)
        self.show_info_act.setShortcut(QKeySequence("Ctrl+I"))
        self.show_info_act.setShortcutContext(Qt.WidgetWithChildrenShortcut)
        self.addAction(self.show_info_act)
