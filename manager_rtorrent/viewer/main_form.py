# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = '$Rev: 159 $:'
__date__ = '$Date: 2015-03-13 21:17:22 +0300 (Fri, 13 Mar 2015) $:'


"""
#############################################################################
##                              Description
##  This application create for copy new torrent file, download from internet,
##  to special folders on Media-server.
##  Where special folders split to categorized:
##      - Music
##      - Video
##      - Applications
##      - Book
##      - Games
##      - Other
##      - And addition catalogs from config file /../config_path.conf
##      on local machine
##
#############################################################################
"""

import os
import shutil
import glob

from PyQt5 import QtWidgets, QtGui, QtCore

from manage_rtorrent.preference.logger import LOGGER
from manage_rtorrent.preference import settings
from manage_rtorrent.tools.db_connection import sql_exec
from manage_rtorrent.preference import querys
from manage_rtorrent.tools.utils import (convert_to_bool, check_exists_file, get_real_tor_name, get_file_media,
                                         get_tor_url)
from manage_rtorrent.resources.lang import messages as msg
from create_add_form import showlabel_new_file, show_error, create_checkbox, MakeButton
from table import TableForm
from show_torrent_info import ShowTorrentInfo
from create_menus import CreateMenu
from manage_rtorrent.preference.settings import DEFAULT_TOR_PATH_ID, EXTENSION

# CONSTANTS
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

# For initiate QApplication in file run.
APP = QtWidgets.QApplication


class DialogWindowForm(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(DialogWindowForm, self).__init__(parent)
        self.index = QtCore.QModelIndex()
        self.widget = QtWidgets.QWidget(self)
        self.setWindowTitle(msg.WINDOW_TITLE)
        self.setWindowIcon(QtGui.QIcon(settings.style['icon']))
        self.widget.setFixedSize(settings.size['width'], settings.size['height'])  # Размеры основной формы.
        self.show_torrent_info = ShowTorrentInfo(self)

        self.popup_menu()
        self.t_form = TableForm(self)
        self.t_form.table.customContextMenuRequested.connect(self.popup_action)
        # self.setFocusProxy(self.t_form.table)

        self.block_mk_main_layout()
        self.block_mk_count_label()
        self.block_mk_button()
        self.block_mk_combobox()
        self.block_cancel_button()
        self.block_checkbox_del_file()

        # ---- LAYOUT FOR TABLE FORM ----#
        layout = QtWidgets.QHBoxLayout(self)
        layout.addWidget(self.widget)
        layout.addWidget(self.t_form.table)

        # ---- ACTIONS FOR OBJECTS ----#
        # Double click to select object.
        # self.t_form.item_model.connect(self.t_form.table, QtCore.SIGNAL("doubleClicked(QModelIndex)"), self.double_clicked)

    def block_mk_main_layout(self):
        """Создание глобальной схемы расстановки(компановки) объектов в форме"""
        self.mainLayout = QtWidgets.QGridLayout(self.widget)
        self.spacerItem = QtWidgets.QSpacerItem(20, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.layout_cancel = QtWidgets.QHBoxLayout(self.widget)
        self.layout_empty_label = QtWidgets.QHBoxLayout(self.widget)

        self.mainLayout.addLayout(self.layout_empty_label, 4, 0, 1, 1)
        self.layout_cb_allow_del_tor = QtWidgets.QHBoxLayout(self.widget)
        self.mainLayout.addLayout(self.layout_cb_allow_del_tor, 6, 1, 1, 1)
        self.mainLayout.addItem(self.spacerItem, 5, 1, 2, 1)
        self.mainLayout.addLayout(self.layout_cancel, 6, 0, 1, 2)

    def block_mk_button(self):
        """BUTTON BLOCK"""
        count_of_button = sql_exec(querys.get_all_id_button)
        main_button = []
        for i_button in range(1, count_of_button):
            button = MakeButton(self, sql=querys.get_param_button, sql_params=i_button)
            button.action(self.move_torrent, button.path)
            main_button.append(button)

        # Auto range layout button.
        for l_but in range(len(main_button)):
            row = ((3 + (l_but - 1)) / 2)
            column = (((l_but - 1) - 1) % 2)
            self.mainLayout.addWidget(main_button[l_but], row, column, 1, 1)

    def block_mk_combobox(self):
        """# ---- COMBOBOX BLOCK ----#"""
        self.combo_box = QtWidgets.QComboBox(self)
        self.combo_box.setMaximumWidth(settings.size_combobox['width'])
        self.combo_box.addItem('')
        self.combo_box.activated.connect(self.hold_data_combo_box)
        count_comboitems = sql_exec(querys.get_all_id_combobox) + 1
        for combo_item in range(1, count_comboitems):
            self.combo_box.addItem(sql_exec(querys.get_param_name_combobox_from_id.format(combo_item)))

        self.mainLayout.addWidget(self.combo_box, 4, 1, 1, 1)

    def block_cancel_button(self):
        """Блок создания кнопки отмены"""
        self.pb_alternate_path = MakeButton(sql=querys.get_param_button, sql_params=settings.OTHER_FOLDER_ID,
                                            set_action=self.alternate_path_moving)
        self.pb_cancel = MakeButton(sql=querys.get_param_button, sql_params=settings.EXIT_ID,
                                    set_action=QtWidgets.qApp.quit)
        self.pb_cancel.setAutoDefault(True)
        self.pb_cancel.setDefault(True)
        self.layout_cancel.addWidget(self.pb_cancel, alignment=QtCore.Qt.AlignCenter)
        self.layout_empty_label.addWidget(self.pb_alternate_path, alignment=QtCore.Qt.AlignRight)

    def block_checkbox_del_file(self):
        """Блок создания checkbox для разрешения удаления файлов"""
        self.cb_del_torrent = create_checkbox('Удаление файлов', 'Allow_file_del',
                                              convert_to_bool(sql_exec(querys.get_pref_param.format('delete_file_on_start'))),
                                              'Разрешить удаление файлов двойным кликом')
        self.layout_cb_allow_del_tor.addWidget(self.cb_del_torrent, alignment=QtCore.Qt.AlignRight)

    def block_mk_count_label(self):
        """# ---- LABEL BLOCK----#"""
        self.lb_length_list_files = showlabel_new_file()  # Создание ярлыка отображения новых файлов.
        self.mainLayout.addWidget(self.lb_length_list_files, 0, 0, 1, 2)

    # ---- POPUP ----#
    def popup_menu(self):
        """Создание контекстного меню для работы с файлами"""
        self.menu = QtWidgets.QMenu()
        self.view_action = self.menu.addAction(msg.MENU_SHOW_INFO)
        self.delete_action = self.menu.addAction(msg.MENU_DEL_FILE)
        self.quit_action = self.menu.addAction(msg.MENU_EXIT)

    def popup_action(self, pos):
        """Действия с контекстным меню"""
        for index in self.t_form.table.selectionModel().selection().indexes():
            table_model = self.t_form.table.model()
            column = 0
            path = table_model.item(index.row(), column).text()

        action = self.menu.exec_(self.t_form.table.viewport().mapToGlobal(pos))
        if action == self.quit_action:
            QtWidgets.qApp.quit()
        elif action == self.view_action:
            self.fill_widget(get_file_media(path), pos)
        elif action == self.delete_action:
            self.delete_select_position()

    def delete_select_position(self):
        """Удаление выбранных позиций в таблице"""
        try:
            files_t = self.get_selection()
            LOGGER.debug('Выбраны файлы для удаления: %s', files_t)
            for tor_f in files_t:
                self.delete_file(tor_f)
        except UnboundLocalError:
            pass

    def show_info_of_file(self):
        """Показать информацию о файлах в нутри torrent"""
        files_t = self.get_selection()
        LOGGER.debug('Выбран файл для просмотрата: %s', files_t)
        self._fill_widget_info_show(get_file_media(str(files_t[0])))

    def _fill_widget_info_show(self, list_files):
        """
            Заполняет форму @listWidget из @popup_action метода если в
            метаданных torrent`а имеется ключ 'files'
        """
        if list_files:
            self.show_torrent_info.clear()
            for x in list_files:
                self.show_torrent_info.addItem(_fromUtf8(x))
            self.show_torrent_info.set_top_level_window()
            self.show_torrent_info.move_to_center()
            self.show_torrent_info.raise_()
            self.show_torrent_info.show()
        else:
            self.show_torrent_info.clear()
            self.show_torrent_info.close()

    def fill_widget(self, list_files, pos):
        """
            Заполняет форму @listWidget из @popup_action метода если в
            метаданных torrent`а имеется ключ 'files'
        """
        if list_files:
            self.show_torrent_info.clear()
            for x in list_files:
                self.show_torrent_info.addItem(_fromUtf8(x))
            pos.setY(pos.y() + 260 + self.height())
            pos.setX(pos.x() + 310 + self.width())
            self.show_torrent_info.move(pos)
            self.show_torrent_info.set_top_level_window()
            self.show_torrent_info.move_to_center()
            self.show_torrent_info.raise_()
            self.show_torrent_info.show()
        else:
            self.show_torrent_info.clear()
            self.show_torrent_info.close()

    def change_length_label(self):
        """"Изменение написания лэйбла"""
        list_new_files = glob.glob(os.path.join(sql_exec(querys.get_tor_path.format(DEFAULT_TOR_PATH_ID)), EXTENSION))
        if list_new_files:
            self.lb_length_list_files.setText(QtWidgets.QApplication.translate('Form', settings.HTML_HEAD_CENTER.format(
                msg.COUNT_T_FILES.format(len(list_new_files)), None)))
            self.lb_length_list_files.setStyleSheet("QLabel#length_label {{color: {0}}}".format(settings.color['lime']))
        else:
            self.lb_length_list_files.setText(QtWidgets.QApplication.translate('Form', settings.HTML_HEAD_CENTER.format(
                msg.NO_NEW_FILES), None,))
            self.lb_length_list_files.setStyleSheet("QLabel#length_label {{color: {0}}}".format(settings.color['red']))

    #---- ACTIONS ----#
    def move_torrent(self, dst_folder):
        """Копирование исходника торрента в необходимый каталог"""
        try:
            if self.cb_del_torrent.checkState() == 0:
                files_t = self.get_selection()
                LOGGER.debug('SELECTED FILES: %s', files_t)
                if files_t:
                    for tor_file in files_t:
                        dst_file = os.path.join(dst_folder, os.path.basename(tor_file))
                        if check_exists_file(dst_file):
                            os.remove(dst_file)
                            # Данный лог приводит к ошибке копирования и остановки программы, из за того что он
                            # не может прочитать имя файла или что то подобное.
                            LOGGER.info('File "{0}" is remove from destination directory {1}'.format(os.path.basename(tor_file), dst_folder))
                        shutil.move(tor_file, dst_folder)
                        LOGGER.info(msg.FILE_MOVE.format(os.path.basename(tor_file), dst_folder))
                    self.close()
                    QtWidgets.qApp.quit()
        except Exception as er:
            LOGGER.exception(er)
            LOGGER.error(er)
            self.close()
            show_error(er, self)

    def hold_data_combo_box(self, item):
        """
            Сохранить текущее состояние выбранного элемента в ComboBox
            Так же не позволяет закрыться программе после выбора строки списка.
            :param item: выбранный пункт в comboBox списке
        """
        self.select_item_cb = item

    def alternate_path_moving(self):
        """
            Перенести файлы из дополнительных путей
        """
        try:
            path = sql_exec(querys.get_param_combobox_from_id.format(self.select_item_cb))
            LOGGER.debug('Начало перемещения файла в каталог: "{0}"'.format(path))
            self.move_torrent(path)  # get item from dict and put copyfile
            LOGGER.debug('Файл перемещен в каталог: "{0}"'.format(path))
            self.close()
            QtWidgets.qApp.quit()
        except AttributeError as er:
            LOGGER.error(er)
            show_error(er, self)

    def change_state(self):
        """
            Изменить состояние checkbox удаления файлов.
        """
        state = self.cb_del_torrent.checkState()
        if state == 0:
            self.cb_del_torrent.setCheckState(QtCore.Qt.Checked)
        else:
            self.cb_del_torrent.setCheckState(QtCore.Qt.Unchecked)

    # ---- WORK WITH TORRENT FILES ----#
    def double_clicked(self, model_index):
        """
            Current select item on table.
            :type : qModelIndex
        """
        row = int(model_index.row())
        column = 0  # Table 0 = Full path.
        table_model = self.t_form.table.model()
        path = table_model.item(row, column).text()
        self.remove_torrent(path)

    def find_torrent_file(self):
        """Поиск файлов и заполнение таблицы"""
        torrent_dir = glob.glob(os.path.join(sql_exec(querys.get_tor_path.format(DEFAULT_TOR_PATH_ID)), EXTENSION))
        for x in torrent_dir:
            self.fill_table(x)

    def fill_table(self, arg):
        """
            Вне зависимости от того сколько было создано таблиц выше,
            здесь создает новые таблицы
            :arg : list all torrent files.
        """
        path = arg
        name = get_real_tor_name(arg)
        url = get_tor_url(arg)
        self.t_form.item_model.appendRow([
            QtGui.QStandardItem(path),
            QtGui.QStandardItem(name),
            QtGui.QStandardItem(url)])

    def remove_torrent(self, torrent):
        """Метод проверки разрешения на удаление файла по двойному клику
        :param torrent: файл торрент для удаления
        """
        if self.cb_del_torrent.isChecked():
            self.delete_file(torrent)

    def delete_file(self, t_file):
        """Удаление выбранного файла
        :param t_file: путь до файла
        """
        if t_file:
            try:
                LOGGER.info(msg.FILE_REMOVE.format(t_file))
                os.remove(t_file)
                self.t_form.clear_table()
                self.find_torrent_file()
                self.change_length_label()
            except Exception as er:
                LOGGER.error(er)
                show_error(er, self)

    #---- GET SELECTED ROW (INDEX`ES) ----#
    def get_selection(self):
        """Получить выбранную позицию"""
        # index = self.theSelectionModel.selectedIndexes()
        select_model = self.t_form.table.selectionModel()
        index = select_model.selectedIndexes()
        selected_items = []
        for x in index:
            table = 0
            row = x.row()
            table_model = self.t_form.table.model()
            path = table_model.item(row, table).text()
            selected_items.append(str(path))
        return selected_items

    def set_current_index(self):
        # Set auto-visible index (row).
        row = 0
        coll = 1
        idx = self.t_form.item_model.index(row, coll, QtCore.QModelIndex())
        self.t_form.table.setCurrentIndex(idx)


class MainWindow(QtWidgets.QMainWindow, CreateMenu):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        CreateMenu.__init__(self)
        self.main_form = DialogWindowForm()
        self.setWindowTitle('SyncTorrent')
        self.setCentralWidget(self.main_form)
        self.main_form.t_form.table.hide()
        # self.focusNextChild()
        self.setFocusProxy(self.main_form.t_form.table)

        self._closed = True
        self._static = convert_to_bool(sql_exec(querys.get_pref_param.format('show_info_on_start')))
        self._maxwidth = self.maximumWidth()
        self.setMinimumSize(self.size())
        self.layout().setSizeConstraint(QtWidgets.QLayout.SetFixedSize)

        #---- ANIMATION METHOD ON START -----#
        self.animator = QtCore.QParallelAnimationGroup(self)
        for item in (self, self.main_form):
            animation = QtCore.QPropertyAnimation(item, 'maximumWidth')
            animation.setDuration(100)
            animation.setEasingCurve(QtCore.QEasingCurve.OutCubic)
            self.animator.addAnimation(animation)
        self.animator.finished.connect(self.slide_window_static)

        #---- BUTTON MANAGE ANIMATION ----#
        self.pb_hide_table = QtWidgets.QPushButton(msg.HIDE_INFO)
        self.pb_hide_table.clicked.connect(self.set_slide_window_button)
        self.main_form.mainLayout.addWidget(self.pb_hide_table, 8, 1, alignment=QtCore.Qt.AlignRight)
        self.pb_hide_table.setToolTip('Показать информацию о torrent файле')


        #---- SHORTCUT ----#
        self.slide_act.triggered.connect(self.set_slide_window_button)
        self.refresh_act.triggered.connect(self.refresh_table)
        self.select_all_act.triggered.connect(lambda: self.main_form.t_form.table.selectColumn(1))
        self.del_file_act.triggered.connect(self.main_form.delete_select_position)
        self.show_info_act.triggered.connect(self.main_form.show_info_of_file)

        self.create_actions()
        self.create_menu()

    def set_slide_window_button(self):
        """
            To establish the basic properties of the animation window.
        """
        for index in range(self.animator.animationCount()):
            animation = self.animator.animationAt(index)
            width = animation.targetObject().width()
            animation.setStartValue(width)
            if self._closed:
                self.main_form.t_form.table.show()
                animation.setEndValue(width)
                self.pb_hide_table.setText(msg.HIDE_INFO)
            else:
                self.pb_hide_table.setText(msg.SHOW_INFO)
        self._closed = not self._closed
        self.animator.start()

    def slide_window_static(self):
        """
            Static position window on start.
        """
        if self._closed:
            self.main_form.t_form.table.hide()

    def move_to_center(self):
        """
            Move window to center screen.
        """
        #screen = QtWidgets.QDesktopWidget().screenGeometry()  # Alike with bottom.
        #size = self.geometry()  # Alike with bottom.
        screen = QtWidgets.QApplication.desktop()
        fsize = self.frameSize()
        self.move((screen.width() - fsize.width()) / 2, (screen.height() - fsize.height()) / 2)

    def set_top_level_window(self):
        """
            Set strong on top window on start.
        """
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized | QtCore.Qt.WindowActive)
        self.activateWindow()

    def refresh_table(self, arg=None):
        """
            :incoming data @ arg: is not use.
        """
        self.main_form.change_length_label()
        self.main_form.t_form.clear_table()
        self.main_form.find_torrent_file()
        self.main_form.set_current_index()

    def change_state(self):
        self.main_form.change_state()

