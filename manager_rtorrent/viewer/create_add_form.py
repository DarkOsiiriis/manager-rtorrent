# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QPushButton
import glob
import os

from collections import Sequence

from manage_rtorrent.preference.settings import EXTENSION, DEFAULT_TOR_PATH_ID
from manage_rtorrent.tools.db_connection import sql_exec
from manage_rtorrent.preference.querys import get_tor_path
from manage_rtorrent.preference import querys

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


# ---- MAKE OBJECTS ----#
def create_label(name):
    """Создание лэйбла"""
    label = QtWidgets.QLabel(name)
    label.setObjectName(name)
    label.setText(
        QtWidgets.QApplication.translate("Form",
                                         "<html><head/><body><p align=\"center\">{0}</p></body></html>".format(name),
                                         None))
    return label


class MakeButton(QPushButton):
    def __init__(self, parent=None, *args, **kwargs):
        super(MakeButton, self).__init__(parent)
        self.name = kwargs.get('name', '{0}')  # Прямое имя или шаблон для генерации имени через скрипт.
        self.path = kwargs.get('path')
        self._obj_name = kwargs.get('obj_name')
        self._tooltip = kwargs.get('tooltip')
        # Опциональные параметры.
        self._sql = kwargs.get('sql')  # Необязательный параметр.
        self._sql_params = kwargs.get('sql_params') if self.is_sequence(kwargs.get('sql_params')) else (kwargs.get('sql_params'), )
        self.set_action = kwargs.get('set_action')  # Задать действие для кнопки.
        self.act_param = kwargs.get('act_param')
        # Инициация параметров.
        self.ext_func = None
        self.act_args = None

        # Если есть команда SQL, тогда не запускать команду создания кнопки.
        self.run_sql() if self._sql else self._mk_button()

        if self.set_action:
            self.action(self.set_action)

    def _mk_button(self):
        """Создание кнопки"""
        self.setText(self.name)
        self.setObjectName(self._obj_name)
        self.setToolTip(self._tooltip)
        self.setFlat(True)
        self.setFixedSize(200, 30)

    def run_sql(self):
        """Получить параментры кнопки по средствам прямого sql запроса из класса"""
        pref_button = sql_exec(querys.get_param_button.format(*self._sql_params), outfetch='all')
        self.name = self.name.format(pref_button[0].encode('utf-8'))
        self._obj_name = pref_button[1]
        self.path = pref_button[2]
        self._tooltip = pref_button[3]
        self.id_button = pref_button[4]
        # Создание кнопки.
        self._mk_button()

    def action(self, func, *args):
        """Задать действие при нажатии на кнопку"""
        self.ext_func = func
        self.act_args = args

    def mousePressEvent(self, e):
        """Перехват нажатия на кнопку левой клавишей мыши
        :param e: событие
        """
        if e.button() == Qt.LeftButton:
            self.ext_func(*self.act_args) if self.act_args else self.ext_func()

    @staticmethod
    def is_sequence(seq):
        """Проверка строки на признак последовательности"""
        if isinstance(seq, basestring):
            return False
        return isinstance(seq, Sequence)


def create_checkbox(name, obj_name, state, tooltip):
    """Создание чекбокса

    :param name: имя объекта и имя самого чекбокса
    :param state: булевое значение, состояние чекбокса
    :param tooltip: строка с описанием, подсказкой
    :return: созданный объект QCheckBox
    """
    checkbox = QtWidgets.QCheckBox(name)
    checkbox.setObjectName(obj_name)
    checkbox.setChecked(state)
    checkbox.setStyleSheet("QCheckBox#%s:checked {color: #FF0000}" % obj_name)  # Red color.
    checkbox.setToolTip(tooltip)
    return checkbox


def showlabel_new_file():
    """Показать кол-во новых файлов, а так же задать цвет отображения

    :params list_new_files: список новых файлов торрект
    :return:
    """
    list_new_files = glob.glob(os.path.join(sql_exec(get_tor_path.format(DEFAULT_TOR_PATH_ID)), EXTENSION))
    if list_new_files:
        lb_length_list_files = create_label(_fromUtf8('Количество Torrent файлов - {0}'.format(len(list_new_files))))
        lb_length_list_files.setObjectName('length_label')
        lb_length_list_files.setStyleSheet("QLabel#length_label {color: #00FF00}")  # Lime.
    else:
        lb_length_list_files = create_label(_fromUtf8('Новых файлов нет'))
        lb_length_list_files.setObjectName('length_label')
        lb_length_list_files.setStyleSheet("QLabel#length_label {color: #FF0000}")  # Red.

    return lb_length_list_files


# ---- SHOW ERROR LOGGING ----#
def show_error(msg, parrent=None):
    """
        Show error message in default form @QMessageBox.Critical
        QtWidgets.QMessageBox(self, QtWidgets.QMessageBox.Critical,
                                 windowTitle='ERROR INFORMATION',
                                 text='Critical Error',
                                 standartButtons=QtWidgets.QMessageBox.Ok)
    """
    message = str(msg)
    msg_form = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, _fromUtf8('Информация о ошибки'),
                                     _fromUtf8('Критическая ошибка'), QtWidgets.QMessageBox.Ok, parrent)
    msg_form.setDetailedText(_fromUtf8('Получена ошибка!!!!:\n\n%s' % message.rstrip('\n')))
    msg_form.exec_()  # Give do`t closes form.
    msg_form.open()



