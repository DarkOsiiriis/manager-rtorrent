# -*- coding: utf-8 -*-


from __future__ import print_function

from PyQt5 import QtCore
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QGridLayout, QGroupBox,\
    QLabel, QLineEdit, QPushButton, QVBoxLayout, QFileDialog

from manage_rtorrent.tools.db_connection import sql_exec
from manage_rtorrent.preference.logger import LOGGER
from manage_rtorrent.preference import querys
from shortcuts import GlobalShortcuts

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class ChangeForm(QDialog, GlobalShortcuts):
    """Класс для изменениния свойств кнопок, имя, описание, путь до файлов"""
    def __init__(self, table_name, parent=None):
        super(ChangeForm, self).__init__(parent)
        self.setWindowTitle("Изменение параметров кнопок")
        self.resize(700, 280)
        self.setModal(True)
        self.table_name = table_name

    def resetting(self):
        """Обнуление данных переменных"""
        LOGGER.debug('Обнуление данных переменных')
        self.id_button = None

        self.cur_name_data = None
        self.cur_path_data = None
        self.cur_tooltip_data = None

    def set_main_layout(self):
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        # buttonBox.accepted.connect(self.accept)
        buttonBox.accepted.connect(self.set_new_data)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.gridGroupBox)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)

    def set_main_layout_add_data(self):
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.add_new_data)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.gridGroupBox)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)

    def edit_name_form(self):
        """Создание блока имени кнопки"""
        self.name_line_edit = QLineEdit()
        self.name_line_edit.setText(self.cur_name_data)
        self.name_line_edit.setToolTip(self.cur_name_data)
        name_label = QLabel('Имя кнопки:')
        self.layout.addWidget(name_label, 0, 0)
        self.layout.addWidget(self.name_line_edit, 0, 1)

    def edit_path_form(self):
        """Создание блока изменения пути для кнопки"""
        self.path_line_edit = QLineEdit()
        self.path_line_edit.setText(self.cur_path_data)
        self.path_line_edit.setToolTip(self.cur_path_data)
        path_label = QLabel('Пути для кнопки:')
        path_button = QPushButton('...')
        path_button.setToolTip('Задать стандартный путь')
        path_button.clicked.connect(lambda: self.remap_button_path(self.cur_path_data))
        self.layout.addWidget(path_label, 1, 0)
        self.layout.addWidget(self.path_line_edit, 1, 1)
        self.layout.addWidget(path_button, 1, 2)

    def edit_tooltip_form(self):
        """Создание блока описание кнопки"""
        self.tooltip_line_edit = QLineEdit()
        self.tooltip_line_edit.setText(self.cur_tooltip_data)
        self.tooltip_line_edit.setToolTip(self.cur_tooltip_data)
        tooltip_label = QLabel('Описание кнопки:')
        self.layout.addWidget(tooltip_label, 2, 0)
        self.layout.addWidget(self.tooltip_line_edit, 2, 1)

    def create_grid_group_box(self):
        """Создание группы кнопок"""
        LOGGER.debug('Текущее значение ИМЯ: %s', self.cur_name_data.encode('utf-8'))
        LOGGER.debug('Текущее значение ПУТЬ: %s', self.cur_path_data.encode('utf-8'))
        LOGGER.debug('Текущее значение ОПИСАНИЕ: %s', self.cur_tooltip_data.encode('utf-8'))
        self.gridGroupBox = QGroupBox()
        self.layout = QGridLayout()

        self.edit_name_form()
        self.edit_path_form()
        self.edit_tooltip_form()

        self.layout.setColumnStretch(1, 20)
        self.layout.setColumnStretch(2, 1)
        self.gridGroupBox.setLayout(self.layout)

    def get_current_data(self):
        """Получить текущие настройки кнопок"""
        LOGGER.debug('Текущие параметры, id кнопки: %s', self.id_button)
        sql = """SELECT name, path, tooltip FROM "{0}" WHERE id={1}""".format(self.table_name, self.id_button)
        try:
            res = sql_exec(sql, outfetch='all')
        except Exception:
            res = ['None'] * 3
        self.cur_name_data = res[0]
        self.cur_path_data = res[1]
        self.cur_tooltip_data = res[2]

    def set_new_data(self):
        """Задать новые значения кнопок"""
        sql = """UPDATE {0} SET name='{1}', path='{2}', tooltip='{3}' WHERE id={4}""".format(
            self.table_name,
            self.name_line_edit.text().encode('utf-8'),
            self.path_line_edit.text().encode('utf-8'),
            self.tooltip_line_edit.text().encode('utf-8'),
            self.id_button)
        sql_exec(sql, output=False)
        self.close()

    def add_new_data(self):
        """Добавление новой записи в список combobox"""
        last_id = sql_exec(querys.get_last_id_combobox)
        if last_id:
            last_id = last_id + 1
        else:
            last_id = 1
        sql = """INSERT INTO "params_combobox"("id","name","object_name","path","tooltip")
                    VALUES ({0:d}, "{1}", "{1}", "{2}", "{3}")""".format(
            last_id,
            self.name_line_edit.text().encode('utf-8'),
            self.path_line_edit.text().encode('utf-8'),
            self.tooltip_line_edit.text().encode('utf-8')
        )
        sql_exec(sql, output=False)
        self.close()

    def remap_button_path(self, dir_path):
        """Действие открытия файла"""
        LOGGER.debug('Путь до каталога %s', dir_path.encode('utf-8'))
        self.new_path_button = QFileDialog.getExistingDirectory(self, "Выбирите новый каталог для кнопки: {0}".format(self.cur_name_data.encode('utf-8')),
                                                                dir_path,
                                                                QFileDialog.ShowDirsOnly |
                                                                QFileDialog.DontResolveSymlinks)
        if self.new_path_button:
            LOGGER.debug('Назначен новый путь: {0}'.format(self.new_path_button))
            self.path_line_edit.setText(self.new_path_button)

    def run(self, id_button):
        LOGGER.debug('Входящий параметр кнопки: %s', id_button)
        self.resetting()
        self.id_button = id_button
        self.get_current_data()
        self.create_grid_group_box()
        self.set_main_layout()
        self.show()

    def add_new_item(self):
        """Добавление новой записи в combo"""
        # LOGGER.debug('Входящий параметр кнопки: %s', id_button)
        self.resetting()
        # self.id_button = id_button
        self.get_current_data()
        self.create_grid_group_box()
        self.set_main_layout_add_data()
        self.show()
