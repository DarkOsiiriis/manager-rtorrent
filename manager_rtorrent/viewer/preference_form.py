# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = '$:'
__date__ = '$:'

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QPushButton
from manage_rtorrent.preference.logger import LOGGER
from manage_rtorrent.tools.db_connection import sql_exec
from manage_rtorrent.preference import querys
from manage_rtorrent.tools.utils import convert_to_bool
from manage_rtorrent.resources.lang import messages as msg
from manage_rtorrent.preference import settings
from remapping_button import ChangeForm
from shortcuts import GlobalShortcuts
from manage_rtorrent.preference.settings import DEFAULT_TOR_PATH_ID
from create_add_form import create_label, MakeButton


class PreferenceForm(QtWidgets.QDialog, GlobalShortcuts):
    def __init__(self, parent=None):
        super(PreferenceForm, self).__init__(parent)
        self.setModal(True)
        self.setWindowTitle('Настройки')
        self.setObjectName('PreferenceForm')

        self.resize(settings.size_pref_form['width'], settings.size_pref_form['height'])
        self.spacerItem = QtWidgets.QSpacerItem(5, 10)

        self.set_target_group()
        self.set_startup_group()
        self.set_button_group()
        self.set_combobox_group()

        # MAIN LAYOUT
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addWidget(self.target_group)
        self.main_layout.addWidget(self.startup_group)
        self.main_layout.addWidget(self.button_group)
        self.main_layout.addItem(self.spacerItem)
        # self.main_layout.addStretch(2)
        # self.main_layout.addItem(self.spacerItem)
        self.main_layout.addWidget(self.combobox_group)

    def set_startup_group(self):
        """# GROUPBOX - STARTUP ----#"""
        self.startup_group = QtWidgets.QGroupBox('Настройки запуска программы')
        startup_layout = QtWidgets.QVBoxLayout()
        # Checkbox удаления файлов.
        cb_start_del = QtWidgets.QCheckBox('Разрешить удаление файлов', self)
        cb_start_del.setObjectName('StartDelete')
        cb_start_del.setChecked(convert_to_bool(sql_exec(querys.get_pref_param.format('delete_file_on_start'))))
        cb_start_del.setToolTip('Установить состояние для удаления или чтения таблицы файлов')
        cb_start_del.stateChanged.connect(lambda state: self.change_state(state, 'delete_file_on_start'))
        startup_layout.addWidget(cb_start_del)

        # Checkbox отображения информации.
        startup_layout.addItem(self.spacerItem)
        cb_start_info = QtWidgets.QCheckBox('Отображать окно информации', self)
        cb_start_info.setObjectName('StartInfo')
        cb_start_info.setChecked(convert_to_bool((sql_exec(querys.get_pref_param.format('show_info_on_start')))))
        cb_start_info.setToolTip('Показать окно информации при старте программы')
        cb_start_info.stateChanged.connect(lambda state: self.change_state(state, 'show_info_on_start'))
        startup_layout.addWidget(cb_start_info)
        self.startup_group.setLayout(startup_layout)

    def set_button_group(self):
        """GROUPBOX - Настройка кнопок"""
        self.select_item_cb = None
        self.button_group = QtWidgets.QGroupBox('Настройка кнопок')
        count_of_button = sql_exec(querys.get_all_id_button) + 1
        main_button = []
        for i_button in range(1, count_of_button):
            button = MakeButton(self, sql=querys.get_param_button, sql_params=i_button, name='<{}> ...')
            button.action(self.remap_button_path, i_button, 'params_button')
            main_button.append(button)

        button_layout = QtWidgets.QGridLayout(self)
        for l_button in range(len(main_button)):
            row = ((3 + (l_button - 1)) / 2)
            column = (((l_button - 1) - 1) % 2)
            button_layout.addWidget(main_button[l_button], row, column)
        self.button_group.setLayout(button_layout)

    def set_target_group(self):
        """GROUPBOX - целевая директория"""
        self.target_group = QtWidgets.QGroupBox('Целевой каталог')
        default_path = sql_exec(querys.get_tor_path.format(DEFAULT_TOR_PATH_ID))
        target_label = create_label('<font size="1">{0}</font>'.format(default_path))
        target_button = MakeButton(self, name='Изменить путь к каталогу ...', path=default_path,
                                   tooltip='Изменение путь до каталога где будет производиться поиск новых файлов torrent')
        target_button.action(self.remap_button_path, DEFAULT_TOR_PATH_ID, 'params_button')

        target_layout = QtWidgets.QVBoxLayout()
        target_layout.addWidget(target_label)
        target_layout.addWidget(target_button)
        self.target_group.setLayout(target_layout)

    def set_combobox_group(self):
        """Настройки COMBOBOX группы"""
        self.combobox_group = QtWidgets.QGroupBox('Дополнительные каталоги')
        cb_layout = QtWidgets.QGridLayout()
        combo_box = QtWidgets.QComboBox(self)
        combo_box.setMaximumWidth(settings.size_combobox['width'])
        combo_box.addItem('')
        combo_box.activated.connect(self.hold_data_combo_box)
        count_comboitems = sql_exec(querys.get_all_id_combobox) + 1
        for combo_item in range(1, count_comboitems):
            combo_box.addItem(sql_exec(querys.get_param_name_combobox_from_id.format(combo_item)))
        cb_layout.addWidget(combo_box, 0, 2)
        path_button = QPushButton('Edit... -->')
        path_button.setToolTip('Задать стандартный путь')
        path_button.clicked.connect(lambda: self.remap_button_path(self.select_item_cb, 'params_combobox'))

        new_group_button = QPushButton('Add New Item')
        new_group_button.setToolTip('Добавить новую запись')
        new_group_button.clicked.connect(lambda: self.new_item_combobox('params_combobox'))

        cb_layout.addWidget(path_button, 0, 0)
        cb_layout.addWidget(new_group_button, 0, 1)
        self.combobox_group.setLayout(cb_layout)

    def hold_data_combo_box(self, item):
        """
            Сохранить текущее состояние выбранного элемента в ComboBox
            Так же не позволяет закрыться программе после выбора строки списка.
            :param item: выбранный пункт в comboBox списке
        """
        self.select_item_cb = item

    def move_to_center(self):
        """
            Move window to center screen.
        """
        screen = QtWidgets.QApplication.desktop()
        size = self.frameSize()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def change_state(self, state, name):
        if state == QtCore.Qt.Checked:
            LOGGER.debug('Checked True: Update info db on TRUE: %s', state)
            sql_exec("""UPDATE startup_option SET params='{1}' WHERE name = '{0}'""".format(name, state), output=False)
        else:
            LOGGER.debug('Checked False: Update info db on FALSE: %s', state)
            sql_exec("""UPDATE startup_option SET params='{1}' WHERE name = '{0}'""".format(name, state), output=False)

    def remap_button_path(self, num_button, table_name):
        """Действие открытия файла"""
        if num_button:
            show_remap_info = ChangeForm(table_name)
            show_remap_info.run(num_button)

    def new_item_combobox(self, table_name):
        """Действие открытия файла"""
        # if num_button:
        show_remap_info = ChangeForm(table_name)
        show_remap_info.add_new_item()

