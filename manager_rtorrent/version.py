# -*- coding: utf-8 -*-

"""
Current auto_update version constant plus version pretty-print method.

"""

__email__ = 'kolesnikov_bn@gw.tander.ru'
__dependence__ = ['PyQt5', 'sql_adapter', 'fdb']
__author__ = '$Author: kolesnikov_bn $'
__date__ = '$Date: 2015-02-24 08:36:12 +0300 (Вт, 24 фев 2015) $'

# REVISION = revision.replace('$', '').replace('Rev:', '').strip()
verbose_version = (0, 0, 1, "beta", 1001)


def get_version(form='short'):
    """
    Return a version string for this package, based on `VERSION`.

    Takes a single argument, ``form``, which should be one of the following
    :param form:
    strings:

    * ``branch``: just the major + minor, e.g. "0.9", "1.0".
    * ``short`` (default): compact, e.g. "0.9rc1", "0.9.0". For package
      filenames or SCM tag identifiers.
    * ``normal``: human readable, e.g. "0.9", "0.9.1", "0.9 beta 1". For e.g.
      documentation site headers.
    * ``verbose``: like ``normal`` but fully explicit, e.g. "0.9 final". For
      tag commit messages, or anywhere that it's important to remove ambiguity
      between a branch and the first final release within that branch.
    """
    # Setup
    versions = {}
    branch = "{0}.{1}".format(verbose_version[0], verbose_version[1])
    tertiary = verbose_version[2]
    type_ = verbose_version[3]
    final = (type_ == "final")
    rev_num = verbose_version[4]
    firsts = "".join([x[0] for x in type_.split()])

    # Branch
    versions['branch'] = branch

    # Short
    v = branch
    if tertiary or final:
        v += "." + str(tertiary)
    if not final:
        v += firsts
    versions['short'] = v

    # Normal
    v = branch
    if tertiary:
        v += "." + str(tertiary)
    if not final:
        v += " " + type_
    versions['normal'] = v

    # Verbose
    v = branch
    if tertiary:
        v += "." + str(tertiary)
    if not final:
        v += " " + type_
        if rev_num:
            v += " " + str(rev_num)
    else:
        v += " final"
    versions['verbose'] = v

    try:
        return versions[form]
    except KeyError:
        raise TypeError('"{0}" is not a valid form specifier.'.format(form))


VERSION = get_version('short')
