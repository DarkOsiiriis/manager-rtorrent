# -*- coding: utf-8 -*-

""" Дополнительные инструменты которые не входят в основные модули программы"""

from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'

import os
import bencode

from manage_rtorrent.preference.logger import LOGGER
from manage_rtorrent.preference.settings import pattern_replace_tor


def convert_to_bool(data):
    """Конвертирование входящего значения в булевое

    :param data: значение для аля псевно булевое значенипе
    :return: boolean value
    """
    state = ['True', '2']
    return True if data in state else False


def check_exists_file(full_path):
    """Проверка наличия файла"""
    return os.path.exists(full_path)


def get_tor_basename(tor_file):
    """Получить имя файла торрента

    :param tor_file: полный путь до файла
    :return: basename
    """
    return pattern_replace_tor.sub('', os.path.splitext(os.path.basename(tor_file))[0])


def get_real_tor_name(tor_file):
    """Получить настроящее имя файла торрент, как на сайте

    :param tor_file: полный путь до файла
    :return: настроящее имя
    """
    with open(tor_file, 'rb') as torrent_file:
        meta_info = bencode.bdecode(torrent_file.read())
        info = meta_info['info']
        name = info['name']
        return name


def get_file_media(tor_file):
    """Получить все вложенные файлы в файле (альбом -> список всех файлов в нем)

    :param tor_file: полный путь до файла
    :return: список файлов в медиа файл
    """
    with open(tor_file, 'rb') as torrent_file:
        meta_data = bencode.bdecode(torrent_file.read())
    info = meta_data['info']
    list_file = []
    # if info.has_key('files'):  # Проверка, может и не отработать нормально.
    if 'files' in info:
        files = info.get('files')
        for item in files:
            for path in item['path']:
                list_file.append(path)
        return list_file


def get_tor_url(tor_file):
    """Получить URL откуда был скачать файл торрент

    :param tor_file: полный путь до файла
    :return: URL
    """
    with open(tor_file, 'rb') as torrent_file:
        meta_info = bencode.bdecode(torrent_file.read())
        return meta_info.get('comment', 'No URL')


