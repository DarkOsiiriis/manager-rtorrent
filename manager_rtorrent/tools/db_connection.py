# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'


import threading
from sql_adapter import ManageSQL
from manage_rtorrent.preference.settings import params_db
from manage_rtorrent.preference.logger import LOGGER


def connect_to_database(query, db=params_db, outfetch='one', output=False, charset='UTF8'):
    """
        Основное инкапсулированное подключение к любым базам.
        Используется для всех видов подключение БД макетов или auto-update.

        :param db: база данных для подключения
        :param outfetch: вывод множесто или одиночная запись
        :param query: запрос для выполения в БД
        :param output: параметр для вывода информации из БД если это select
        :return: вывод информации после запроса к БД
    """
    result = None
    with threading.RLock():
        with ManageSQL(driver='sqlite') as con:
            con.connection(db, charset=charset)
            con.query(query)
            if output:
                result = con.get_output(output=outfetch)[0]
                # result = con.get_output()
            else:
                con.commit()
    return result


def sql_exec(sql, outfetch='one', output=True):
    """Выполнение SQL запроса из БД"""
    return connect_to_database(sql, outfetch=outfetch, output=output)


def sql_exec_script(script, db=params_db, charset='UTF8'):
    """
        Основное инкапсулированное подключение к любым базам.
        Используется для всех видов подключение БД макетов или auto-update.

        :param db: база данных для подключения
        :param outfetch: вывод множесто или одиночная запись
        :param query: запрос для выполения в БД
        :param output: параметр для вывода информации из БД если это select
        :return: вывод информации после запроса к БД
    """
    result = None
    with threading.RLock():
        with ManageSQL(driver='sqlite') as con:
            con.connection(db, charset=charset)
            con.query_script    (script)
            con.commit()
    return result
