# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'


WINDOW_TITLE                = 'Sync Torrents'
# MENU MESSAGE.
MENU_SHOW_INFO              = 'Показать информацию'
MENU_DEL_FILE               = 'Удалить файл'
MENU_EXIT                   = 'Выход'

# Other.
COUNT_T_FILES               = 'Количество Torrent файлов - {0}'
NO_NEW_FILES                = 'Новых файлов нет'
FILE_MOVE                   = 'Файл {0} перемещен в целевую директорию {1}'
en_FILE_MOVE                = 'File "{0}" is move to destination directory {1}'
FILE_REMOVE                 = 'Файл удален: {0}'
HIDE_INFO                   = 'Скрыть инфо'
SHOW_INFO                   = 'Показать инфо'
ABOUT                       = '<font color="{0[1]}"><h2><tt>{0[0]}</tt></h2></font> Утилита предназначена для копирования новых торрент файлов в необходимую директорию. <pre><font color="{1[1]}">Version: {1[0]}</font></pre>'
