# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'

# Получение параметров из БД для панели настроек.
get_pref_param = """SELECT params FROM startup_option WHERE name='{0}'"""
get_tor_path = """SELECT path FROM params_button WHERE id = {0}"""
get_param_button = """SELECT name, object_name, path, tooltip, id FROM params_button WHERE id={0:d}"""
get_all_id_button = """SELECT Count(id) FROM params_button WHERE global_button='True'"""
get_param_combobox = """SELECT path FROM params_combobox WHERE object_name = '{0}'"""
get_param_combobox_from_id = """SELECT path FROM params_combobox WHERE id = {0}"""
get_param_name_combobox_from_id = """SELECT name FROM params_combobox WHERE id = {0}"""
get_all_id_combobox = """SELECT Count(id) FROM params_combobox"""
get_last_id_combobox = """SELECT max(id) FROM params_combobox"""
get_path_param_button = """SELECT path FROM params_button WHERE id = {0}"""


# Создание новой БД.
NEW_DB = """
CREATE TABLE "params_button"(
    "id" Integer,
    "name" Text,
    "object_name" Text,
    "path" Text,
    "tooltip" Text,
    "global_button" Text );


INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 1,'One Button','pb_one','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 2,'Two Button','pb_two','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 3,'Three Button','pb_three','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 4,'Four Button','pb_four','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 5,'Five Button','pb_five','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 6,'Six Button','pb_six','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 7,'Sever Button','pb_seven','None','Описание для кнопки','True' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 8,'Выход','pb_eight','None','Выход из программы','False' );
INSERT INTO "params_button"("id","name","object_name","path","tooltip","global_button") VALUES ( 9,'Стандартный путь','pb_nine','~/Downloads','','False' );


CREATE TABLE "params_combobox"(
    "id" Integer,
    "name" Text,
    "object_name" Text,
    "path" Text,
    "tooltip" Text );


CREATE TABLE "startup_option"(
    "id" Integer NOT NULL,
    "name" Text,
    "params" Text );

CREATE INDEX "pref_pane_id_idx" ON "startup_option"( "id" );

INSERT INTO "startup_option"("id","name","params") VALUES ( 1,'delete_file_on_start','0' );
INSERT INTO "startup_option"("id","name","params") VALUES ( 2,'show_info_on_start','0' );

"""
