# -*- coding: utf-8 -*-

from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'

import os
import re


PROGRAM_NAME = 'Manager rTorrent'
# Параметры логирования.
DEBUGGING_MODE = True
log_config = {'level': DEBUGGING_MODE and 'DEBUG' or 'INFO',
              'log_path': os.path.join(os.path.expanduser('~'), 'Desktop', 'manage_rtorrent.txt')}

# Параметры конфигураций.
db = 'manager_rtorrent.db'
config_path = os.path.join(os.path.expanduser('~'), 'Application Data', PROGRAM_NAME)
params_db = os.path.join(config_path, db)

# Стиль приложения, тема оформления.
style = {'main_theme': 'manage_rtorrent/resources/theme_win8_flat.qss',
         'default': 'Plastique',
         'icon': 'images/SyncTorrent.png'}

# Расширение файлов и путь до поиска новый torrent файлов.
EXTENSION = '*.torrent'

# Настройка размеров основной формы.
size = {'width': 550,
        'height': 260,
        'offset': 350}

# Настройки размеров дополнительного окна информации о файлах.
size_file_info = {'width': 556,
                  'height': 292,
                  'x': 1,
                  'y': 1}

size_pref_form = {'width': 300,
                  'height': 580}

size_combobox = {'width': 200}

# Для расположения строки в label.
HTML_HEAD_CENTER = '<html><head/><body><p align="center">{0}</p></body><html>'

# ====================================================================================

pattern_replace_tor = re.compile('\[\w+(-?\.?\w+?)+\]')

# Color HEX.
color = {'lime': '#00FF00',
         'red': '#FF0000',
         'orange': '#FF7F00',
         'green': '#00FF00'}

# Константы ID button для поиска в БД.
DEFAULT_TOR_PATH_ID = 9
OTHER_FOLDER_ID = 7
EXIT_ID = 8
