# -*- coding: utf-8 -*-


from __future__ import print_function

__author__ = 'bohrman'
__revision__ = ' $'
__date__ = '$'


import logging
from logging.config import dictConfig as dictconfig
from settings import log_config
from logger_formatter import LOG_SETTINGS

# Параментры форматирования журнала.
dictconfig(LOG_SETTINGS)
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(hasattr(logging, log_config['level']))

