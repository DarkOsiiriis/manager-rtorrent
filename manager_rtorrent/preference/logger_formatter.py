# -*- coding: utf-8 -*-

"""
    Описание файла.
"""

# Определение функции print.
from __future__ import print_function

# Constants.
__author__ = '$Author: bohrman $'
__email__ = 'kolesnikov_bn@magnit.ru'
__date__ = '$Date: 2015-03-03 23:19:17 +0300 (Tue, 03 Mar 2015) $'
revision = '$Revision: 146 $'

import settings


LOG_SETTINGS = {
    'version': 1,
    'root': {
        'level': settings.log_config['level'],
        'handlers': ['console', 'file'],
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': settings.log_config['level'],
            'formatter': 'detailed',
            'stream': 'ext://sys.stdout',
        },
        # Ротация файла по размеру 10 МБ.
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': settings.log_config['level'],
            'formatter': 'detailed',
            'filename': settings.log_config['log_path'],
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        },
    },
    'formatters': {
        'detailed': {
            'format': '[%(asctime)-8s] [%(threadName)-10s] %(levelname)-8s %(module)-24s [LINE:%(lineno)-4d] >> %(message)s',
            'datefmt': '%Y-%m-%d %X ',
        },
    },
}
